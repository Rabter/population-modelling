#ifndef COEFFTABLE_H
#define COEFFTABLE_H

#include <vector>

template <typename T>
class CoeffTable
{
public:
    CoeffTable(int start, int step, const std::vector<T> &coef_table, unsigned approx_degree = 0);

    T real(int age) const;
    T smoothed(int age) const;

protected:
    int _start, _step;
    std::vector<std::vector<T>> table;
    std::vector<double> equation;
};

#endif // COEFFTABLE_H
