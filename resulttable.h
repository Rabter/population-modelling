#ifndef RESULTTABLE_H
#define RESULTTABLE_H

#include <QDialog>
#include <vector>

namespace Ui {
class ResultTable;
}

class ResultTable : public QDialog
{
    Q_OBJECT

public:
    explicit ResultTable(const std::vector<std::vector<int>> &result, int year, QWidget *parent = nullptr);
    ~ResultTable();

private:
    Ui::ResultTable *ui;
};

#endif // RESULTTABLE_H
