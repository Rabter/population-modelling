#include "inttextedit.h"


IntTextEdit::IntTextEdit(QWidget *parent): ValidTextEdit(parent, "[+-]?(?:\\d+)") {}

void IntTextEdit::add_value(const QString &strval)
{
    values.push_back(strval.toInt());
}

void IntTextEdit::clear_values()
{
    values.clear();
}

bool IntTextEdit::get(int index, int &value)
{
    if (validate())
    {
        make_correct();
        value = values[index];
        return true;
    }
    make_incorrect();
    return false;
}

bool IntTextEdit::get(std::vector<int> &values)
{
    if (validate())
    {
        values = this->values;
        return true;
    }
    return false;
}
