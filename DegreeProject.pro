QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    approximation/approximation.cpp \
    coefftable.cpp \
    doubletextedit.cpp \
    intentry.cpp \
    inttextedit.cpp \
    main.cpp \
    mainwindow.cpp \
    model.cpp \
    resulttable.cpp \
    validentry.cpp \
    validtextedit.cpp

HEADERS += \
    approximation/approximation.h \
    coefftable.h \
    doubletextedit.h \
    intentry.h \
    inttextedit.h \
    mainwindow.h \
    model.h \
    resulttable.h \
    statistics.h \
    validentry.h \
    validtextedit.h

FORMS += \
    mainwindow.ui \
    resulttable.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
