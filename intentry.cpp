#include "intentry.h"

IntEntry::IntEntry(QWidget *parent): ValidEntry(parent, "[+-]?(?:\\d+)") {}

bool IntEntry::get_int(int &num)
{
    if (validate())
    {
        QString input = this->text();
        num = input.toInt();
        return true;
    }
    return false;
}
