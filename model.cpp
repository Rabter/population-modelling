#include "model.h"

void Model::run_calc(std::vector<std::vector<int> > &n)
{
    n.clear();
    int num = 1;
    for (int i = 0; num > 0; ++i)
    {
        num = initital.value(i);
        n.push_back(std::vector<int>(max_year, num));
    }
    int max_age = n.size() - 1;
    const int tau = 1, h = 1;
    for (int j = 0; j < max_year - 1; ++j)
    {
        for (int i = 0; i < max_age - 1; ++i)
            n[i + 1][j + 1] = n[i + 1][j] + (f.value(i, j) - death_stats.value(i) * n[i][j] - (n[i + 1][j] - n[i][j]) / h) * tau;

        n[0][j + 1] = 0;
        for (int y = low_fertile_age; y < hight_fertile_age; ++y)
            n[0][j + 1] += k.value(y) * n[y][j] * female_percent.value(y);
    }
}
