#include <fstream>
#include "resulttable.h"
#include "ui_resulttable.h"
#define MAX_WIDTH 1000
#define MAX_HEIGHT 700

ResultTable::ResultTable(const std::vector<std::vector<int>> &result, int year, QWidget *parent):
    QDialog(parent),
    ui(new Ui::ResultTable)
{
    ui->setupUi(this);
    int n = result.size() + 1, m = result[0].size() + 1;
    int h = n * ui->table->verticalHeader()->defaultSectionSize(), w = m * ui->table->horizontalHeader()->defaultSectionSize();

    if (w > MAX_WIDTH)
        w = MAX_WIDTH;
    if (h > MAX_HEIGHT)
        h = MAX_HEIGHT;

    resize(w + 2 * ui->table->x() + 20, h + 2 * ui->table->y());

    ui->table->resize(w, h);
    ui->table->setRowCount(n + 1);
    ui->table->setColumnCount(m);

    std::ofstream fout("res.txt");
    fout << '.';

    for (unsigned i = 0; i < result[0].size(); ++i, ++year)
    {
        ui->table->setItem(0, i + 1, new QTableWidgetItem(QString::number(year)));
        fout << '\t' << year;
    }

    for (unsigned i = 0; i < result.size(); ++i)
    {
        ui->table->setItem(i + 1, 0, new QTableWidgetItem(QString::number(i)));
        fout << '\n' << i;
        for (unsigned j = 0; j < result[i].size(); ++j)
        {
            ui->table->setItem(i + 1, j + 1, new QTableWidgetItem(QString::number(result[i][j])));
            fout << '\t' << result[i][j];
        }
    }

    ui->table->setItem(result.size() + 1, 0, new QTableWidgetItem("Всего"));
    fout << "\nВсего";

    for (unsigned i = 0; i < result[0].size(); ++i)
    {
        int sum = 0;
        for (unsigned j = 0; j < result.size(); ++j)
            sum += result[j][i];
        ui->table->setItem(result.size() + 1, i + 1, new QTableWidgetItem(QString::number(sum)));
        fout << '\t' << sum;
    }

    fout.close();
}

ResultTable::~ResultTable()
{
    delete ui;
}
