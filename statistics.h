#ifndef STATISTICS_H
#define STATISTICS_H

#include "coefftable.h"

template <typename T>
class Stats
{
public:
    Stats(const CoeffTable<T> &table, bool smooth = false): rude(!smooth), table(table) {}

    inline void smooth(bool val) { rude = !val; }
    inline T value(int index) const { return (rude? table.real(index) : table.smoothed(index)); }

protected:
    bool rude;
    CoeffTable<T> table;
};

template <typename T1, typename T2>
class DoubleStats
{
public:
    DoubleStats(const CoeffTable<T1> &table1, const CoeffTable<T2> &table2, bool smooth1 = false, bool smooth2 = false):
        rude1(!smooth1), rude2(!smooth2), table1(table1), table2(table2) {}

    inline void smooth(bool val1, bool val2) { rude1 = !val1; rude2 = !val2; }
    inline double value(int index1, int index2) const { return (rude1? table1.real(index1): table1.smoothed(index1)) * (rude2? table2.real(index2): table2.smoothed(index2)); }

protected:
    bool rude1, rude2;
    CoeffTable<T1> table1;
    CoeffTable<T2> table2;
};

#endif // STATISTICS_H
