#ifndef DOUBLETEXTEDIT_H
#define DOUBLETEXTEDIT_H

#include "validtextedit.h"

class DoubleTextEdit: public ValidTextEdit
{
public:
    DoubleTextEdit(QWidget *parent);
    bool get(int index, double &value);
    bool get(std::vector<double> &values);

protected:
    std::vector<double> values;

    void add_value(const QString &strval) override;
    void clear_values() override;
};

#endif // DOUBLETEXTEDIT_H
