#include "doubletextedit.h"


DoubleTextEdit::DoubleTextEdit(QWidget *parent): ValidTextEdit(parent, "[+-]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)") {}

void DoubleTextEdit::add_value(const QString &strval)
{
    values.push_back(strval.toDouble());
}

void DoubleTextEdit::clear_values()
{
    values.clear();
}

bool DoubleTextEdit::get(int index, double &value)
{
    if (validate())
    {
        make_correct();
        value = values[index];
        return true;
    }
    make_incorrect();
    return false;
}

bool DoubleTextEdit::get(std::vector<double> &values)
{
    if (validate())
    {
        values = this->values;
        return true;
    }
    return false;
}
