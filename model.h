#ifndef RUNNIN_CALCULATION_H
#define RUNNIN_CALCULATION_H

#include <vector>
#include "statistics.h"

struct Model
{
    Stats<int> initital;
    Stats<double> female_percent;
    Stats<double> k;
    Stats<double> death_stats;
    DoubleStats<double, double> f;

    int max_year;

    int low_fertile_age;
    int hight_fertile_age;


    void run_calc(std::vector<std::vector<int>> &n);
};



#endif // RUNNIN_CALCULATION_H
