#include <cmath>
#include <fstream>
#include "coefftable.h"
#include "approximation/approximation.h"

template<typename T>
CoeffTable<T>::CoeffTable(int start, int step, const std::vector<T> &coef_table, unsigned approx_degree): _start(start), _step(step), table(0)
{
    int age = start;
    double shifted_age = start + step * 0.5;
    std::vector<std::vector<double>> shifted_table(0);
    for (T coef: coef_table)
    {
        table.push_back(std::vector<T>{static_cast<T>(age), static_cast<T>(coef), 1});
        shifted_table.push_back(std::vector<double>{static_cast<double>(shifted_age), static_cast<double>(coef), 1});
        age += step;
        shifted_age += step;
    }
    if (!approx_degree)
        approx_degree = table.size() - 1;
    equation = equasion_coeffitients(shifted_table, approx_degree);
}

template<typename T>
T CoeffTable<T>::real(int age) const
{
    unsigned int i = (age > _start? (age - _start) / _step : 0);
    if (i >= table.size())
        i = table.size() - 1;
    return static_cast<T>(table[i][1]);
}

template<typename T>
T CoeffTable<T>::smoothed(int age) const
{
    double res = equation[0];
    for (unsigned i = 1; i < equation.size(); ++i)
        res += equation[i] * pow(age, i);
    return res;
}

template class CoeffTable<int>;
template class CoeffTable<double>;
