#ifndef APPROXIMATION_H
#define APPROXIMATION_H

#include <vector>

std::vector<double> equasion_coeffitients(const std::vector<std::vector<double> > &table, int n);

#endif // APPROXIMATION_H
