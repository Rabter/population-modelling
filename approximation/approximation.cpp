#include <cmath>
#include "approximation.h"

void get_slau_matrix(std::vector<std::vector<double>> &matrix, const std::vector<std::vector<double>> &table, unsigned n)
{
    unsigned size = table.size();
    std::vector<double> col(n + 1, 0);
    matrix.resize(n + 1, col);

    for (unsigned m = 0; m < n + 1; ++m)
        for (unsigned i = 0; i < size; ++i)
        {
            double tmp = table[i][2] * pow(table[i][0], m);
            for (unsigned k = 0; k < n + 1; ++k)
                matrix[m][k] += tmp * pow(table[i][0], k);
            col[m] += tmp * table[i][1];
        }

    for (unsigned i = 0; i < n + 1; ++i)
        matrix[i].push_back(col[i]);
}

void solve_matrix(std::vector<double> &res, std::vector<std::vector<double>> &matrix)
{
    unsigned size = matrix.size();
    for (unsigned k = 0; k < size; ++k)
        for (unsigned i = k + 1; i < size; ++i)
        {
            double coeff = -(matrix[i][k] / matrix[k][k]);
            for (unsigned j = k; j < size + 1; ++j)
                matrix[i][j] += coeff * matrix[k][j];
        }

    res.resize(size, 0);
    for (int i = size - 1; i > -1; --i)
    {
        for (int j = size - 1; j > i; --j)
            matrix[i][size] -= res[j] * matrix[i][j];
        res[i] = matrix[i][size] / matrix[i][i];
    }
}

std::vector<double> equasion_coeffitients(const std::vector<std::vector<double>> &table, int n)
{
    std::vector<double> res;
    std::vector<std::vector<double>> matrix;

    get_slau_matrix(matrix, table, n);
    solve_matrix(res, matrix);

    return res;
}
