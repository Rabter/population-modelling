#include <fstream>
#include <QTableWidget>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "statistics.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->entry_pop_file->set_reg_exp(".+");
    ui->entry_fp_file->set_reg_exp(".+");
    ui->entry_birth_file->set_reg_exp(".+");
    ui->entry_death_file->set_reg_exp(".+");
    ui->entry_ym_file->set_reg_exp(".+");
    ui->entry_am_file->set_reg_exp(".+");

    result_table = nullptr;
}

MainWindow::~MainWindow()
{
    if (result_table)
        delete result_table;
    delete ui;
}

bool MainWindow::save_table(IntEntry *start, IntEntry *step, ValidTextEdit *table, const std::string &filename)
{
    std::ofstream fout(filename);
    int start_val, step_val;

    if (!(start->get_int(start_val) && step->get_int(step_val) && table->validate()))
        return false;

    fout << start_val << " " << step_val << "\n";
    fout << table->toPlainText().toStdString();

    fout.close();
    return true;
}

bool MainWindow::load_table(IntEntry *start, IntEntry *step, ValidTextEdit *table, const std::string &filename)
{
    std::ifstream fin(filename);
    if (fin.is_open())
    {

        std::string buf;
        getline(fin, buf);
        QStringList start_step = QString::fromStdString(buf).split(" ");
        start->setText(start_step[0]);
        step->setText(start_step[1]);

        QString text = "";
        while (getline(fin, buf))
            text += QString::fromStdString(buf) + "\n";
        text.resize(text.size() - 1);
        table->setPlainText(text);

        fin.close();
        return true;
    }
    else
        return false;
}

bool MainWindow::fill_table_values(int &start, int &step, std::vector<int> &age_table, IntEntry *start_entry, IntEntry *step_entry, IntTextEdit *table)
{
    if (!(start_entry->get_int(start) && step_entry->get_int(step)))
        return false;

    if (!table->get(age_table))
        return false;
    return  true;
}

bool MainWindow::fill_table_values(int &start, int &step, std::vector<double> &age_table, IntEntry *start_entry, IntEntry *step_entry, DoubleTextEdit *table)
{
    if (!(start_entry->get_int(start) && step_entry->get_int(step)))
        return false;

    if (!table->get(age_table))
        return false;

    return  true;
}

bool MainWindow::get_input(Model *&input)
{
    bool ok = true;

    int start, step, start2, step2, start_year, finish_year, low, high;
    std::vector<int> table_int;
    std::vector<double> table_double, table_double2;

    ok = fill_table_values(start, step, table_int, ui->entry_pop_start, ui->entry_pop_step, ui->te_pop) && ok;
    table_int.push_back(0);
    Stats<int> pop(CoeffTable<int>(start, step, table_int), false);

    ok = fill_table_values(start, step, table_double, ui->entry_fp_start, ui->entry_fp_step, ui->te_fp) && ok;
    Stats<double> fp(CoeffTable<double>(start, step, table_double), false);

    ok = fill_table_values(start, step, table_double, ui->entry_birth_start, ui->entry_birth_step, ui->te_birth) && ok;
    Stats<double> birth(CoeffTable<double>(start, step, table_double), false);
    low = start;
    high = low + step * table_double.size();

    ok = fill_table_values(start, step, table_double, ui->entry_death_start, ui->entry_death_step, ui->te_death) && ok;
    Stats<double> death(CoeffTable<double>(start, step, table_double), false);

    ok = fill_table_values(start, step, table_double, ui->entry_ym_start, ui->entry_ym_step, ui->te_ym) && ok;
    ok = fill_table_values(start2, step2, table_double2, ui->entry_am_start, ui->entry_am_step, ui->te_am) && ok;
    DoubleStats<double, double> migration(CoeffTable<double>(start, step, table_double, 1), CoeffTable<double>(start2, step2, table_double2), true, false);

    ok = ui->entry_start_year->get_int(start_year) && ok;
    ok = ui->entry_finish_year->get_int(finish_year) && ok;

    input = new Model {
        pop,
        fp,
        birth,
        death,
        migration,
        finish_year - start_year + 1,
        low,
        high,
    };

    return ok;
}

void MainWindow::on_btn_save_pop_clicked()
{
    QString filename;
    ui->entry_pop_file->get_raw(filename);
    save_table(ui->entry_pop_start, ui->entry_pop_step, ui->te_pop, filename.toStdString());
}

void MainWindow::on_btn_load_pop_clicked()
{
    if (load_table(ui->entry_pop_start, ui->entry_pop_step, ui->te_pop, ui->entry_pop_file->text().toStdString()))
            ui->entry_pop_file->setStyleSheet("background:#fff;");
        else
            ui->entry_pop_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_save_fp_clicked()
{
    QString filename;
    ui->entry_fp_file->get_raw(filename);
    save_table(ui->entry_fp_start, ui->entry_fp_step, ui->te_fp, filename.toStdString());
}


void MainWindow::on_btn_load_fp_clicked()
{
    if (load_table(ui->entry_fp_start, ui->entry_fp_step, ui->te_fp, ui->entry_fp_file->text().toStdString()))
            ui->entry_fp_file->setStyleSheet("background:#fff;");
        else
            ui->entry_fp_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_save_bitrh_clicked()
{
    QString filename;
    ui->entry_birth_file->get_raw(filename);
    save_table(ui->entry_birth_start, ui->entry_birth_step, ui->te_birth, filename.toStdString());
}

void MainWindow::on_btn_load_birth_clicked()
{
    if (load_table(ui->entry_birth_start, ui->entry_birth_step, ui->te_birth, ui->entry_birth_file->text().toStdString()))
            ui->entry_birth_file->setStyleSheet("background:#fff;");
        else
            ui->entry_birth_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_save_death_clicked()
{
    QString filename;
    ui->entry_death_file->get_raw(filename);
    save_table(ui->entry_death_start, ui->entry_death_step, ui->te_death, filename.toStdString());
}

void MainWindow::on_btn_load_death_clicked()
{
    if (load_table(ui->entry_death_start, ui->entry_death_step, ui->te_death, ui->entry_death_file->text().toStdString()))
        ui->entry_death_file->setStyleSheet("background:#fff;");
    else
        ui->entry_death_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_save_ym_clicked()
{
    QString filename;
    ui->entry_ym_file->get_raw(filename);
    save_table(ui->entry_ym_start, ui->entry_ym_step, ui->te_ym, filename.toStdString());
}

void MainWindow::on_btn_load_ym_clicked()
{
    if (load_table(ui->entry_ym_start, ui->entry_ym_step, ui->te_ym, ui->entry_ym_file->text().toStdString()))
        ui->entry_ym_file->setStyleSheet("background:#fff;");
    else
        ui->entry_ym_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_save_am_clicked()
{
    QString filename;
    ui->entry_am_file->get_raw(filename);
    save_table(ui->entry_am_start, ui->entry_am_step, ui->te_am, filename.toStdString());
}

void MainWindow::on_btn_load_am_clicked()
{
    if (load_table(ui->entry_am_start, ui->entry_am_step, ui->te_am, ui->entry_am_file->text().toStdString()))
        ui->entry_am_file->setStyleSheet("background:#fff;");
    else
        ui->entry_am_file->setStyleSheet("background:#f88;");
}

void MainWindow::on_btn_calc_clicked()
{
    Model *model;
    std::vector<std::vector<int>> result;

    if (!get_input(model))
        return;

    model->run_calc(result);
    result.pop_back();

    if (result_table)
        delete result_table;

    int year;
    ui->entry_start_year->get_int(year);
    result_table = new ResultTable(result, year);
    result_table->show();
}
