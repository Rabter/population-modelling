#ifndef INTTEXTEDIT_H
#define INTTEXTEDIT_H

#include "validtextedit.h"

class IntTextEdit: public ValidTextEdit
{
public:
    IntTextEdit(QWidget *parent);
    bool get(int index, int &value);
    bool get(std::vector<int> &values);

protected:
    std::vector<int> values;

    void add_value(const QString &strval) override;
    void clear_values() override;
};

#endif // INTTEXTEDIT_H
