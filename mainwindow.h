#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <string>
#include <QMainWindow>
#include "intentry.h"
#include "inttextedit.h"
#include "doubletextedit.h"
#include "model.h"
#include "resulttable.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_save_pop_clicked();
    void on_btn_load_pop_clicked();
    void on_btn_save_fp_clicked();
    void on_btn_load_fp_clicked();
    void on_btn_save_bitrh_clicked();
    void on_btn_load_birth_clicked();
    void on_btn_save_death_clicked();
    void on_btn_load_death_clicked();    
    void on_btn_save_ym_clicked();
    void on_btn_load_ym_clicked();
    void on_btn_save_am_clicked();
    void on_btn_load_am_clicked();
    void on_btn_calc_clicked();

private:
    Ui::MainWindow *ui;
    ResultTable *result_table;

    bool save_table(IntEntry *start, IntEntry *step, ValidTextEdit *table, const std::string &filename);
    bool load_table(IntEntry *start, IntEntry *step, ValidTextEdit *table, const std::string &filename);
    bool fill_table_values(int &start, int &step, std::vector<int> &age_table, IntEntry *start_entry, IntEntry *step_entry, IntTextEdit *table);
    bool fill_table_values(int &start, int &step, std::vector<double> &age_table, IntEntry *start_entry, IntEntry *step_entry, DoubleTextEdit *table);
    bool get_input(Model *&input);
};
#endif // MAINWINDOW_H
