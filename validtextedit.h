#ifndef VALIDTEXTEDIT_H
#define VALIDTEXTEDIT_H

#include <vector>
#include <QTextEdit>

class ValidTextEdit: public QTextEdit
{
    Q_OBJECT
public:
    ValidTextEdit(QWidget *parent, QString expression);
    int validate();

protected:
    short valid;
    QString expression;
    virtual void add_value(const QString &strval) = 0;
    virtual void clear_values() = 0;
    void make_incorrect();
    void make_correct();

protected slots:
    void reset_validation();
};

#endif // VALIDTEXTEDIT_H
