#include "validtextedit.h"

ValidTextEdit::ValidTextEdit(QWidget *parent, QString expression): QTextEdit(parent), expression(expression)
{
    connect(this, SIGNAL(textChanged()), this, SLOT(reset_validation()));
    valid = -1;
}

void ValidTextEdit::make_incorrect()
{
    this->setStyleSheet("background:#f88;");
}

void ValidTextEdit::make_correct()
{
    this->setStyleSheet("");
}

void ValidTextEdit::reset_validation()
{
    valid = -1;
}

int ValidTextEdit::validate()
{
    if (valid == -1)
    {
        clear_values();
        QRegExp correct(expression);
        QStringList raw = this->toPlainText().split("\n");
        for (int i = 0; i < raw.size() && valid; ++i)
        {
            QString line = raw[i];
            if ((valid = correct.exactMatch(line)))
                add_value(line);
        }
    }
    if (valid)
        make_correct();
    else
        make_incorrect();
    return valid;
}
