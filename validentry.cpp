#include "validentry.h"

ValidEntry::ValidEntry(QWidget *parent, const QString &expression) : QLineEdit(parent), expression(expression)
{
    connect(this, SIGNAL(textChanged(QString)), this, SLOT(reset_validation()));
    valid = -1;
}

void ValidEntry::make_incorrect()
{
    this->setStyleSheet("background:#f88;");
}

void ValidEntry::make_correct()
{
    this->setStyleSheet("");
}

void ValidEntry::reset_validation()
{
    valid = -1;
}

void ValidEntry::set_reg_exp(QString exp)
{
    expression = exp;
}

bool ValidEntry::get_raw(QString &string)
{
    if (validate())
    {
        string = this->text();
        return true;
    }
    return false;
}

int ValidEntry::validate()
{
    if (valid == -1)
    {
        QRegExp correct(expression);
        valid = correct.exactMatch(this->text());
    }
    if (valid)
        make_correct();
    else
        make_incorrect();
    return valid;
}
